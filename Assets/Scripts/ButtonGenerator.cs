﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonGenerator : MonoBehaviour
{
    private void Start()
    {
        generateNumber();
    }
    [ContextMenu("Gen")]
    public void generateNumber()
    {
        int index = 0;
        foreach(Transform child in transform)
        {
            foreach (Transform subchild in child)
            {
                subchild.transform.GetChild(0).GetComponent<Text>().text = index.ToString("D2");
                index++;
            }
        }
    }
}
