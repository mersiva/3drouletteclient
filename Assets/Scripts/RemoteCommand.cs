﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class RemoteCommand : MonoBehaviour
{
    public static string betAmount = "0";
    public static int command = 0;
    public int buttonCommand;
    public static string player = "1";
    public InputField inputField;
    private void Awake()
    {

    }
    private void Start()
    {

    }
    public void Action_SendTextToServer()
    {

        FMNetworkManager.instance.SendToServer(player.ToString() + ":" + transform.GetChild(0).GetComponent<Text>().text + ":" + betAmount);
    }

    public void setPlayer(int index)
    {
        player = index.ToString();
    }

    public void setBet()
    {
        betAmount = inputField.text.ToString();
    }

    public void resetGame()
    {
        FMNetworkManager.instance.SendToServer(transform.GetChild(0).GetComponent<Text>().text);
    }

    public void setFov(float s)
    {
        FMNetworkManager.instance.SendToServer("Fov:"+s.ToString());
    }
    public void setDof(float s)
    {
        FMNetworkManager.instance.SendToServer("Dof:" + s.ToString());
    }
    public void sendSpin()
    {
     
        FMNetworkManager.instance.SendToServer(transform.GetChild(0).GetComponent<Text>().text + ":" + inputField.text.ToString());
    }

    public void setCam(int index)
    {

        FMNetworkManager.instance.SendToServer("Cam:"+index.ToString());
    }


    public void changeCam()
    {

        FMNetworkManager.instance.SendToServer("ChangeCamera");

    }
    public void Action_SendByteToServer(int _value)
    {
        FMNetworkManager.instance.SendToServer(new byte[_value]);
    }
}

