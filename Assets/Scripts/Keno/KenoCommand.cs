﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class KenoCommand : MonoBehaviour
{
    public static int command = 0;
    public int buttonCommand;
    public static string player = "1";
    public InputField inputField;
    public Dropdown dropDown;
    public static string currentColor = "black";


    public void newGame()
    {
        FMNetworkManager.instance.SendToServer("NewGame");
    }

    public void showAvatar()
    {
        FMNetworkManager.instance.SendToServer("ShowAvatar");
    }

    public void hideAvatar()
    {
        FMNetworkManager.instance.SendToServer("HideAvatar");
    }

    public void drawBall()
    {
        if(int.Parse(inputField.text) > 0 && int.Parse(inputField.text) <= 99)
        {

            //byte[] _bytes = System.Text.Encoding.UTF8.GetBytes("DrawBall:" + inputField.text + ":" + currentColor);
            FMNetworkManager.instance.SendToServer("DrawBall:" + inputField.text + ":" + currentColor);
            Debug.LogError("DrawBall:" + inputField.text + ":" + currentColor);
        }
   
    }

    public void setColor(string s)
    {

        currentColor =s;
    }





}

