﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class VideoPresentorCommand : MonoBehaviour
{
    public Dropdown ball1Color;
    public InputField ball1No;

    public Dropdown ball2Color;
    public InputField ball2No;

    public Dropdown ball3Color;
    public InputField ball3No;

    public Dropdown ball4Color;
    public InputField ball4No;

    public Dropdown ball5Color;
    public InputField ball5No;

    public Dropdown ball6Color;
    public InputField ball6No;



    public InputField videoFileName;



    public void send()
    {
        string sendCommand = "PlayVideo:"+videoFileName.text + ":" + ball1Color.options[ball1Color.value].text + "," + ball1No.text + "," + ball2Color.options[ball2Color.value].text + "," + ball2No.text + "," + ball3Color.options[ball3Color.value].text + "," + ball3No.text + "," + ball4Color.options[ball4Color.value].text + "," + ball4No.text +
            "," + ball5Color.options[ball5Color.value].text + "," + ball5No.text + "," + ball6Color.options[ball6Color.value].text + "," + ball6No.text;
        Debug.LogError(sendCommand);
            //byte[] _bytes = System.Text.Encoding.UTF8.GetBytes("DrawBall:" + inputField.text + ":" + currentColor);
            FMNetworkManager.instance.SendToServer(sendCommand);

   
    }

}

